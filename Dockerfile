
FROM nginx:alpine

ARG NGINX_PORT=80

RUN echo -e "server {\n  listen 80;\n resolver 127.0.0.11 ipv6=off;\n server_name localhost 127.0.0.1;\n location / {\nproxy_pass_header Authorization;\n  proxy_pass http://HIDDEN_SERVER:$NGINX_PORT;\n  proxy_set_header Host \$host;\n  proxy_set_header X-Real-IP \$remote_addr;\n  proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;\n  proxy_http_version 1.1;\n  proxy_set_header Connection \"\";\n  proxy_buffering off;\n  client_max_body_size 0;\n  proxy_read_timeout 36000s;\n  proxy_redirect off;\n }\n}\n" > /etc/nginx/conf.d/default.conf
